# Pluriapi

This project was generated with Laravel version 6.0.1.

# Steps to start development server

run 'composer install'  
create .env file. you can use .env.example as example.  
run 'php artisan migrate:install'  
run 'php artisan migrate'  
run 'php artisan db:seed'  

## Development server

Run 'php artisan serve' for a dev server. Navigate to 'http://localhost:8000/'.
