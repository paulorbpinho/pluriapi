<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Aluno;
use Faker\Generator as Faker;

$factory->define(Aluno::class, function (Faker $faker) {
    return [
        'nome'            => $faker->name, 
        'email'           => $faker->email, 
        'sexo'            => $faker->randomElement(['masculino','feminino']), 
        'data_nascimento' => $faker->date
    ];
});
