<?php

namespace App\Http\Controllers;

use App\Aluno;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AlunosController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $alunos = Aluno::query();
      if($request->get('nome')) {
        $alunos->where('nome', 'LIKE', "%{$request->get('nome')}%");
      }
      if($request->get('email')) {
        $alunos->where('email', 'LIKE', "%{$request->get('email')}%");
      }
      return $alunos->paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->getValidator($request);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }
        else {
            return Aluno::create($request->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Aluno  $aluno
     * @return \Illuminate\Http\Response
     */
    public function show(Aluno $aluno)
    {
        return $aluno;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Aluno  $aluno
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Aluno $aluno)
    {
        $validator = $this->getValidator($request);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }
        else {
            $aluno->update($request->all());
            return $aluno;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Aluno  $aluno
     * @return \Illuminate\Http\Response
     */
    public function destroy(Aluno $aluno)
    {
        $aluno->delete();

        return response()->json(null, 204);
    }

    private function getValidator(Request $request)
    {
        return Validator::make($request->all(), [
            'nome'            => 'required',
            'email'           => 'required|email:rfc',
            'sexo'            => 'in:masculino,feminino',
            'data_nascimento' => 'required|date'
        ]);
    }
}
