<?php

namespace App\Http\Controllers;

use App\Matricula;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MatriculasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Matricula::with(['curso','aluno'])->paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->getValidator($request);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }
        else {
            return Matricula::create($request->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Matricula  $matricula
     * @return \Illuminate\Http\Response
     */
    public function show(Matricula $matricula)
    {
        return $matricula->load(['curso','aluno']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Matricula  $matricula
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Matricula $matricula)
    {
        $validator = $this->getValidator($request);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }
        else {
            $matricula->update($request->all());
            return $matricula;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Matricula  $matricula
     * @return \Illuminate\Http\Response
     */
    public function destroy(Matricula $matricula)
    {
        $matricula->delete();

        return response()->json(null, 204);
    }

    private function getValidator(Request $request)
    {
        return Validator::make($request->all(), [
            'aluno_id'          => 'required|exists:alunos,id',
            'curso_id'          => 'required|exists:cursos,id'
        ]);
    }
}
