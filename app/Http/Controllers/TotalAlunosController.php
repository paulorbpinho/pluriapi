<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\TotalAlunosResource;
use App\Matricula;
use App\Aluno;
use App\Curso;

class TotalAlunosController extends Controller
{
    public function index()
    {
      $total_alunos = collect( TotalAlunosResource::collection( Matricula::with(['curso','aluno'])->get() ) );
      $qtd_por_id = $total_alunos->countBy(function ($resource) {
        return $resource['id'];
      });
      return array_values( $total_alunos->unique('id')->map( function($resultado) use ($qtd_por_id) {
        $resultado['quantidade'] = $qtd_por_id[$resultado['id']];
        unset( $resultado['id'] );
        return $resultado;
      })->all() );
    }
}
