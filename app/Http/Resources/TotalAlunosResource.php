<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TotalAlunosResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->curso->id . '|' . $this->aluno->faixa_etaria['id'] . '|' . $this->aluno->sexo,
            'curso'        => $this->curso->titulo,
            'faixa_etaria' => $this->aluno->faixa_etaria['descricao'],
            'sexo'         => $this->aluno->sexo
        ];
    }
}
