<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aluno extends Model
{
    protected $fillable = ['nome', 'email', 'sexo', 'data_nascimento'];
    public $appends = ['idade'];

    public function getIdadeAttribute()
    {
      $data_nascimento = \Carbon\Carbon::parse($this->data_nascimento);
      return \Carbon\Carbon::now()->diffInYears($data_nascimento);
    }

    public function getFaixaEtariaAttribute()
    {
      $faixa_etaria = [];
      if( $this->idade < 15 ) {
          $faixa_etaria['id'] = 1;
          $faixa_etaria['descricao'] = 'Menor que 15 anos';
      } 
      elseif( $this->idade >= 15 && $this->idade <= 18 ) {
          $faixa_etaria['id'] = 2;
          $faixa_etaria['descricao'] = 'Entre 15 e 18 anos';
      }
      elseif( $this->idade >= 19 && $this->idade <= 24 ) {
          $faixa_etaria['id'] = 3;
          $faixa_etaria['descricao'] = 'Entre 19 e 24 anos';
      }
      elseif( $this->idade >= 25 && $this->idade <= 30 ) {
          $faixa_etaria['id'] = 4;
          $faixa_etaria['descricao'] = 'Entre 25 e 30 anos';
      }
      elseif( $this->idade > 30 ) {
          $faixa_etaria['id'] = 5;
          $faixa_etaria['descricao'] = 'Maior que 30 anos';
      }
      return $faixa_etaria;
    }
}
