<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Curso;
use App\Aluno;

class Matricula extends Model
{
    protected $fillable = ['aluno_id', 'curso_id'];

    public function curso()
    {
      return $this->belongsTo(Curso::class);
    }

    public function aluno()
    {
      return $this->belongsTo(Aluno::class);
    }
}
