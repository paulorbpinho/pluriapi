<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->group(function () {
  Route::resource('alunos', 'AlunosController')->except(['create','edit']);
  Route::resource('cursos', 'CursosController')->except(['create','edit']);
  Route::resource('matriculas', 'MatriculasController')->except(['create','edit']);
  Route::get('total_alunos', 'TotalAlunosController@index');
});
