<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Matricula;
use App\Curso;
use App\Aluno;

class MatriculaTest extends TestCase
{
    /**
     * Testar o valor do atributo 'fillable'.
     *
     * @return void
     */
    public function testeFillable()
    {
        $matricula = new Matricula([
            'aluno_id' => Aluno::first()->id,
            'curso_id' => Curso::first()->id
        ]);

        $this->assertTrue($matricula->getFillable() == ['aluno_id', 'curso_id']);
    }  

    /**
     * Testar o valor da relacionamento 'Aluno'.
     *
     * @return void
     */
    public function testeAluno()
    {
        $matricula = new Matricula([
            'aluno_id' => Aluno::first()->id,
            'curso_id' => Curso::first()->id
        ]);

        $this->assertTrue($matricula->aluno instanceof Aluno);
    }  

    /**
     * Testar o valor do relacionamento 'Curso'.
     *
     * @return void
     */
    public function testeCurso()
    {
        $matricula = new Matricula([
            'aluno_id' => Aluno::first()->id,
            'curso_id' => Curso::first()->id
        ]);

        $this->assertTrue($matricula->curso instanceof Curso);
    }  
}
