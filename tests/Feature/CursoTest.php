<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Curso;

class CursoTest extends TestCase
{
    /**
     * Testar o valor do atributo 'fillable'.
     *
     * @return void
     */
    public function testeFillable()
    {
        $curso = new Curso([
            'titulo' => 'Java',
            'descricao' => 'Curso básico de Java.'
        ]);

        $this->assertTrue($curso->getFillable() == ['titulo', 'descricao']);
    }  
}
