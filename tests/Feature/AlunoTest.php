<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Aluno;

class AlunoTest extends TestCase
{
    /**
     * Testar o valor da idade.
     *
     * @return void
     */
    public function testeIdade()
    {
        $aluno = new Aluno([
            'nome' => 'Paulo Pinho',
            'email' => 'paulorbpinho@gmail.com', 
            'sexo' => 'masculino', 
            'data_nascimento' => '09/05/1986'
        ]);

        $this->assertTrue($aluno->idade == \Carbon\Carbon::now()->diffInYears($aluno->data_nascimento) );
    }

    /**
     * Testar o valor da faixa etária.
     *
     * @return void
     */
    public function testeFaixaEtaria()
    {
        $aluno = new Aluno([
            'nome' => 'Paulo Pinho',
            'email' => 'paulorbpinho@gmail.com', 
            'sexo' => 'masculino', 
            'data_nascimento' => '09/05/1986'
        ]);

        $this->assertTrue($aluno->faixa_etaria == [
            'id' => 5,
            'descricao' => 'Maior que 30 anos'
        ]);
    }

    /**
     * Testar o valor do atributo 'fillable'.
     *
     * @return void
     */
    public function testeFillable()
    {
        $aluno = new Aluno([
            'nome' => 'Paulo Pinho',
            'email' => 'paulorbpinho@gmail.com', 
            'sexo' => 'masculino', 
            'data_nascimento' => '09/05/1986'
        ]);

        $this->assertTrue($aluno->getFillable() == ['nome', 'email', 'sexo', 'data_nascimento']);
    }  

    /**
     * Testar o valor do atributo 'appends'.
     *
     * @return void
     */
    public function testeAppends()
    {
        $aluno = new Aluno([
            'nome' => 'Paulo Pinho',
            'email' => 'paulorbpinho@gmail.com', 
            'sexo' => 'masculino', 
            'data_nascimento' => '09/05/1986'
        ]);

        $this->assertTrue($aluno->appends == ['idade']);
    }  
}
